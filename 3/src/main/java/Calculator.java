public class Calculator {
    public double add(double firstNum, double secondNum){
        return firstNum + secondNum;
    }

    public double subtract(double firstNum, double secondNum){
        return firstNum - secondNum;
    }

    public double multiply(double firstNum, double secondNum){
        return firstNum * secondNum;
    }

    public double divide(double firstNum, double secondNum){
        if(secondNum == 0){
            throw new ArithmeticException();
        }
        else {
            return firstNum / secondNum;
        }
    }
}
