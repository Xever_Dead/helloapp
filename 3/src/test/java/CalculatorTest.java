import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void setUp() {
        System.out.println("Before test");
        calculator = new Calculator();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void add() {
        double param1 = 2;
        double param2 = 1;

        double expected = 3;
        double actual = calculator.add(param1, param2);

        assertEquals(expected, actual);
    }

    @Test
    void subtract() {
        double param1 = 2;
        double param2 = 1;

        double expected = 1;
        double actual = calculator.subtract(param1, param2);

        assertEquals(expected, actual);
    }

    @Test
    void multiply() {
        double param1 = 2;
        double param2 = 1;

        double expected = 2;
        double actual = calculator.multiply(param1, param2);

        assertEquals(expected, actual);
    }

    @Test
    void divide() {
        double param1 = 2;
        double param2 = 1;

        double expected = 2;
        double actual = calculator.divide(param1, param2);

        assertEquals(expected, actual);
    }
}