package config;

import SpringClasses.Group;
import SpringClasses.Student;
import SpringClasses.Teacher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
public class ContextConfig {

    @Bean
    public Student student1(){
        return new Student("Ivan", "Ivanov");
    }

    @Bean
    public Student student2(){
        return new Student("Ivan", "Ivanooff");
    }

    @Bean
    public Student student3(){
        return new Student("Vanya", "Vaniev");
    }

    @Bean
    public Student student4(){
        return new Student("Ivanka", "Ivanova");
    }

    @Bean
    public List<Student> students(){
        return Arrays.asList(student1(), student2(), student3(), student4());
    }

    @Bean
    public Group group1(){
        return new Group("Group");
    }

    @Bean
    public Teacher teacher1(){
        return new Teacher();
    }
}
