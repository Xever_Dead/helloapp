package SpringClasses;

public class Student {
    private String firstName;
    private String surname;

    public Student(String firstName, String surname){
        this.firstName = firstName;
        this.surname = surname;
    }

    public void writeName(){
        System.out.println((firstName + " " +  surname));
    }
}
