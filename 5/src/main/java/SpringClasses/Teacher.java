package SpringClasses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Teacher {

    @Autowired
    @Qualifier("group1")
    private Group group;

    public void writeStudents(){
        System.out.println("Список студентов:");
        group.writeStudentNames();
    }
}
