package SpringClasses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private String name;

    @Autowired
    @Qualifier("students")
    private List<Student> students;

    public Group(String name){
        this.name = name;
    }

    public void writeStudentNames(){
        for(Student student : students){
            student.writeName();
        }
    }
}
