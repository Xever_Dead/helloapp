public class FactorialDoWhileAlgorithm implements FactorialAlgorithm {

    @Override
    public int getAlgorithmResult(int argument){
        if(argument <= 0){
            return 0;
        }

        var result = 1;
        var currentNumber = 1;

        do{
            result*=currentNumber;

            currentNumber++;
        }
        while(currentNumber <= argument);

        return result;
    }
}

