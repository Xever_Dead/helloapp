public interface FactorialAlgorithm {
    int getAlgorithmResult(int argument);
}
