public class GCounter {
    public static void main(String[] args) {
        var a = 0;
        var p = 0;
        var m1 = 0.0;
        var m2 = 0.0;

        if (args.length == 4) {
            try {
                a = Integer.parseInt(args[0]);
                p = Integer.parseInt(args[1]);
                m1 = Double.parseDouble(args[2]);
                m2 = Double.parseDouble(args[3]);
            }
            catch(NumberFormatException ex) {
                System.out.println("Entered arguments are of wrong type");
                return;
            }

            if (p == 0 || m1 + m2 == 0.0) {
                System.out.println("Division by zero");
            }
            else {
                var g = 4 * Math.pow(Math.PI, 2) * Math.pow(a, 3) /
                        Math.pow(p, 2) * (m1 + m2);
                System.out.println(g);
            }
        }
        else {
            System.out.println("Wrong amount of arguments");
        }
    }
}
