import java.util.ArrayList;

public class FibonacciForAlgorithm implements FibonacciAlgorithm {

    @Override
    public ArrayList<Integer> getAlgorithmResult(int argument) {
        if (argument <= 0) {
            return null;
        }

        var result = new ArrayList<Integer>();

        result.add(0);

        if (argument == 1) {
            return result;
        }

        result.add(1);

        if (argument == 2) {
            return result;
        }


        for (var counter = 2; counter < argument; counter++) {
            result.add(result.get(counter - 1) + result.get(counter - 2));
        }

        return result;
    }
}

