public class FactorialWhileAlgorithm implements FactorialAlgorithm {

    @Override
    public int getAlgorithmResult(int argument) {
        if (argument <= 0) {
            return 0;
        }

        var result = 1;
        var currentNumber = 1;

        while (currentNumber <= argument)
        {
            result *= currentNumber;

            currentNumber++;
        }


        return result;
    }
}

