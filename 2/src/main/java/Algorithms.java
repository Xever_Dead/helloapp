import java.util.ArrayList;

public class Algorithms {
    public static void main(String[] args) {
        var algorithmId = 0;
        var cycleType = 0;
        var argument = 0;

        if (args.length != 3) {
            System.out.println("Wrong amount of arguments");
        }
        else {
            try {
                algorithmId = Integer.parseInt(args[0]);
                cycleType = Integer.parseInt(args[1]);
                argument = Integer.parseInt(args[2]);
            }
            catch (NumberFormatException ex) {
                System.out.println("Entered arguments are of wrong type");
                return;
            }

            if (algorithmId < 0 || algorithmId > 2) {
                System.out.println("algorithmId must be in the interval [1, 2]");
            }
            else if (cycleType < 0 || cycleType > 3) {
                System.out.println("cycleType must be in the interval [1, 3]");
            }
            else if (argument <= 0) {
                System.out.println("argument must be above zero");
            }
            else{
                var factorialAlgorithms = new ArrayList<FactorialAlgorithm>();
                var fibonacciAlgorithms = new ArrayList<FibonacciAlgorithm>();

                factorialAlgorithms.add(new FactorialWhileAlgorithm());
                factorialAlgorithms.add(new FactorialDoWhileAlgorithm());
                factorialAlgorithms.add(new FactorialForAlgorithm());
                
                fibonacciAlgorithms.add((new FibonacciWhileAlgorithm()));
                fibonacciAlgorithms.add(new FibonacciDoWhileAlgorithm());
                fibonacciAlgorithms.add(new FibonacciForAlgorithm());
                
                if(algorithmId == 1){
                    var result = fibonacciAlgorithms.get(cycleType - 1).getAlgorithmResult(argument);
                    for(var number : result){
                        System.out.println(number);
                    }
                }
                else
                {
                    var result = factorialAlgorithms.get(cycleType - 1).getAlgorithmResult(argument);
                    System.out.println(result);
                }
            }
        }
    }
}
