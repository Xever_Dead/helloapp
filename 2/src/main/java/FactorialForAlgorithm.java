public class FactorialForAlgorithm implements FactorialAlgorithm {

    @Override
    public int getAlgorithmResult(int argument) {
        if (argument <= 0) {
            return 0;
        }

        var result = 1;

        for (var currentNumber = 1; currentNumber <= argument; currentNumber++) {
            result *= currentNumber;
        }


        return result;
    }
}
