import java.util.ArrayList;

public interface FibonacciAlgorithm {
    ArrayList<Integer> getAlgorithmResult(int argument);
}

