public class Program {
    public static void main(String[] args){
        Worker worker = new Worker(null, 90);
        worker.addProfession(Profession.GLAZIER);

        WorkingTeam team = new WorkingTeam(null, 100);
        team.addWorker(worker);

        WorkingTeam team2 = new WorkingTeam(null, 10);
        team2.addWorker(worker);
        team2.addWorker(worker);

        WorkingTeam team3 = new WorkingTeam(null, 20);
        team3.addWorker(worker);

        TeamPicker picker = new TeamPicker(null,null);
        picker.addTeam(team);
        picker.addTeam(team2);
        picker.addTeam(team3);
        picker.addRequirement(Profession.GLAZIER, 1);
        picker.addRequirement(Profession.CARPENTER, 1);

        WorkingTeam winner = picker.pick();

        if(winner != null){
            System.out.println(winner.getCostOfWork());
        }
        else{
            System.out.println("Netu winnera");
        }
    }
}
