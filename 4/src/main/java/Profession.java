public enum Profession {
    FINISHER,
    ELECTRICIAN,
    PAINTER,
    MASON,
    CARPENTER,
    CRANE_OPERATOR,
    GLAZIER,
    ROOFER
}
