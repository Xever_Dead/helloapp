import java.util.HashSet;

public class WorkingTeam {
    private HashSet<Worker> workers;
    private double salariesCost;
    private double costOfWork;

    public WorkingTeam(HashSet<Worker> workers, double additionalCost){
        if(additionalCost >= 0){
            this.costOfWork = additionalCost;
        }
        else{
            this.costOfWork = 0;
        }

        if(workers != null){
            this.workers = workers;

            for(Worker worker : workers){
                salariesCost += worker.getSalary();
            }

            costOfWork += salariesCost;
        }
        else {
            this.workers = new HashSet<>();
        }
    }

    public double getCostOfWork(){
        return costOfWork;
    }

    public boolean setAdditionalCost(double newAdditionalCost){
        if(newAdditionalCost >= 0){
            costOfWork = newAdditionalCost + salariesCost;
            return true;
        }

        return false;
    }

    public HashSet<Worker> getWorkers(){
        return workers;
    }

    public boolean addWorker(Worker worker){
        if(worker== null){
            return false;
        }

        salariesCost += worker.getSalary();
        costOfWork += worker.getSalary();
        return workers.add(worker);
    }

    public boolean removeWorker(Worker worker){
        if(worker == null){
            return false;
        }

        salariesCost -= worker.getSalary();
        costOfWork -= worker.getSalary();
        return workers.remove(worker);
    }
}
