import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;

public class TeamPicker {
    private HashSet<WorkingTeam> teams;
    private HashMap<Profession, Integer> requirements;

    public TeamPicker(HashSet<WorkingTeam> teams, HashMap<Profession, Integer> requirements){
        this.teams = Objects.requireNonNullElseGet(teams, HashSet::new);

        this.requirements = Objects.requireNonNullElseGet(requirements, HashMap::new);
    }

    public WorkingTeam pick(){
        ArrayList<WorkingTeam> chosenTeams = new ArrayList<>();

        HashMap<Profession, Integer> requirements = (HashMap<Profession, Integer>)this.requirements.clone();

        WorkingTeam chosenTeam = null;

        for(WorkingTeam team : teams){
            boolean isTeamFitsRequirements = true;

            for(Worker worker : team.getWorkers()){
                for(Profession profession : worker.getProfessions()){
                    if(requirements.containsKey(profession) && requirements.get(profession) > 0) {
                        requirements.put(profession, requirements.get(profession) - 1);
                    }
                }
            }

            for(Integer workersOfProfessionLeft : requirements.values()){
                if(workersOfProfessionLeft > 0){
                    isTeamFitsRequirements = false;
                    break;
                }
            }

            if(isTeamFitsRequirements){
                chosenTeams.add(team);
            }

            requirements = (HashMap<Profession, Integer>)this.requirements.clone();
        }
        if(chosenTeams.size() > 0) {
            chosenTeam = chosenTeams.get(0);
            for (WorkingTeam team : chosenTeams) {
                if (team.getCostOfWork() < chosenTeam.getCostOfWork()) {
                    chosenTeam = team;
                }
            }
        }

        return chosenTeam;
    }

    public HashSet<WorkingTeam> getTeams(){
        return teams;
    }

    public boolean addTeam(WorkingTeam team){
        if(team == null){
            return false;
        }

        return teams.add(team);
    }

    public boolean removeTeam(WorkingTeam team) {
        if (team == null) {
            return false;
        }

        return teams.remove(team);
    }

    public HashMap<Profession, Integer> getRequirements(){
        return requirements;
    }

    public void addRequirement(Profession profession, Integer workersAmount){
        if(profession == null){
            return;
        }

        requirements.put(profession, workersAmount);
    }

    public void removeRequirement(Profession profession){
        if(profession == null){
            return;
        }

        requirements.remove(profession);
    }
}
