import java.util.HashSet;
import java.util.Objects;

public class Worker {
    private HashSet<Profession> professions;
    private double salary;

    public Worker(HashSet<Profession> professions, double salary){
        if(salary >= 0){
            this.salary = salary;
        }
        else{
            this.salary = 0;
        }

        this.professions = Objects.requireNonNullElseGet(professions, HashSet::new);
    }

    public double getSalary(){
        return salary;
    }

    public boolean setSalary(double newSalary){
        if(newSalary >= 0){
            salary = newSalary;
            return true;
        }

        return false;
    }

    public HashSet<Profession> getProfessions(){
        return professions;
    }

    public boolean addProfession(Profession profession){
        if(profession == null){
            return false;
        }
        return professions.add(profession);
    }

    public boolean removeProfession(Profession profession){
        return professions.remove(profession);
    }
}
